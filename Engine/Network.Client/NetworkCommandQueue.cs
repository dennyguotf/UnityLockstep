﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lockstep.Core.Logic;
using Lockstep.Core.Logic.Interfaces;
using Lockstep.Core.Logic.Serialization;
using Lockstep.Core.Logic.Serialization.Utils;
using Lockstep.Game;
using Lockstep.Network.Messages;                     

namespace Lockstep.Network.Client
{
    /// <summary>
    /// 网络通信指令队列
    /// </summary>
    public class NetworkCommandQueue : CommandQueue
    {
        /// <summary>
        /// 初始化指令处理回调
        /// </summary>
        public event EventHandler<Init> InitReceived;

        public byte LagCompensation { get; set; }
        /// <summary>
        /// 网络对象
        /// </summary>
        private readonly INetwork _network;
        /// <summary>
        /// 所有已定义的指令类型字典
        /// </summary>
        private readonly Dictionary<ushort, Type> _commandFactories = new Dictionary<ushort, Type>();

        public NetworkCommandQueue(INetwork network)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetTypes().Where(type => type.GetInterfaces().Any(intf => intf.FullName != null && intf.FullName.Equals(typeof(ICommand).FullName))))
                {
                    var tag = ((ICommand)Activator.CreateInstance(type)).Tag;
                    if (_commandFactories.ContainsKey(tag))
                    {
                        throw new InvalidDataException($"The command tag {tag} is already registered. Every command tag must be unique.");
                    }
                    _commandFactories.Add(tag, type);
                }
            }

            _network = network;
            _network.DataReceived += NetworkOnDataReceived;            
        }

        /// <summary>
        /// 增加输入（指令），并发送给服务器（包括：序列化、压缩）
        /// </summary>
        /// <param name="input"></param>
        public override void Enqueue(Input input)
        {
            base.Enqueue(input);

            //Tell the server
            var writer = new Serializer();
            writer.Put((byte)MessageTag.Input);
            writer.Put(input.Tick);
            writer.Put(LagCompensation);
            writer.Put(input.Commands.Count());
            writer.Put(input.ActorId);
            foreach (var command in input.Commands)
            {
                writer.Put(command.Tag);
                command.Serialize(writer);
            }

            _network.Send(Compressor.Compress(writer));
        }

        /// <summary>
        /// 接收网络数据包处理
        /// </summary>
        /// <param name="rawData"></param>
        private void NetworkOnDataReceived(byte[] rawData)
        {   
            var data = Compressor.Decompress(rawData);
             
            var reader = new Deserializer(data);
            var messageTag = (MessageTag)reader.GetByte();
            switch (messageTag)
            {
                case MessageTag.Init:
                    var paket = new Init();
                    paket.Deserialize(reader);
                    InitReceived?.Invoke(this, paket);
                    break;
                case MessageTag.Input:
                    var tick = reader.GetUInt() + reader.GetByte(); //Tick + LagCompensation
                    var countCommands = reader.GetInt();
                    var actorId = reader.GetByte();
                    var commands = new ICommand[countCommands];
                    for (var i = 0; i < countCommands; i++)
                    {
                        var tag = reader.GetUShort();
                        if (!_commandFactories.ContainsKey(tag))
                        {
                            continue;
                        }

                        var newCommand = (ICommand)Activator.CreateInstance(_commandFactories[tag]);
                        newCommand.Deserialize(reader);
                        commands[i] = newCommand;
                    }


                    base.Enqueue(new Input(tick, actorId, commands));
                    break;    
            }
        }   
    }
}