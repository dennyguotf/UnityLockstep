﻿namespace Lockstep.Network
{
    public enum MessageTag : byte
    {
        Init,      //房间初始化信息
        Frame,
        Input,
        HashCode,
        Pulse      //心跳
    }
}