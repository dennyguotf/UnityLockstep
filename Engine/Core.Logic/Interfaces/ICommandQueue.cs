﻿using System.Collections.Generic;

namespace Lockstep.Core.Logic.Interfaces
{
    /// <summary>
    /// 指令队列接口
    /// </summary>
    public interface ICommandQueue
    {
        void Enqueue(Input input);

        List<Input> Dequeue();
    }
}