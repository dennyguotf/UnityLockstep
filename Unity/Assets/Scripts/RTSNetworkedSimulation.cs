﻿            
using System;
using System.Collections;              
using System.IO;
using Lockstep.Common.Logging;
using Lockstep.Core.Logic.Interfaces;
using Lockstep.Core.Logic.Serialization.Utils;
using Lockstep.Game;                      
using Lockstep.Network.Client;
using UnityEngine;           
                              
/// <summary>
/// 
/// </summary>
public class RTSNetworkedSimulation : MonoBehaviour
{      
    public static RTSNetworkedSimulation Instance;

    public string ServerIp = "127.0.0.1";
    public int ServerPort = 9050;

    /// <summary>
    /// 逻辑模拟器（运行、回滚、预测）
    /// </summary>
    public Simulation Simulation;           
    public RTSEntityDatabase EntityDatabase;
    /// <summary>
    /// 是否已连接
    /// </summary>
    public bool Connected => _client.Connected;       
    /// <summary>
    /// 房间内所有角色ID
    /// </summary>
    public byte[] AllActorIds { get; private set; }
    /// <summary>
    /// 网络指令队列
    /// </summary>
    private NetworkCommandQueue _commandQueue;
    /// <summary>
    /// 网络通信客户端对象
    /// </summary>
    private readonly LiteNetLibClient _client = new LiteNetLibClient();

    private void Awake()
    {                                
        Instance = this;

        Log.OnMessage += (sender, args) => Debug.Log(args.Message);

        _commandQueue = new NetworkCommandQueue(_client)
        {
            LagCompensation = 10
        };

        _commandQueue.InitReceived += (sender, init) =>
        {
            AllActorIds = init.AllActors;
            Debug.Log($"Starting simulation. Total actors: {init.AllActors.Length}. Local ActorID: {init.ActorID}");
            Simulation.Start(init.SimulationSpeed, init.ActorID, init.AllActors);
        };

        Simulation = new Simulation(Contexts.sharedInstance, _commandQueue, new UnityGameService(EntityDatabase));      
    }             


    public void DumpGameLog()
    {
        Simulation.DumpGameLog(new FileStream(@"C:\Log\" + Math.Abs(Contexts.sharedInstance.gameState.hashCode.value) + ".bin", FileMode.Create, FileAccess.Write));                   
    }

    /// <summary>
    /// 增加指令
    /// </summary>
    /// <param name="command"></param>
    public void Execute(ICommand command)
    {
        Simulation.Execute(command);
    }

    private void Start()
    {
        _client.Start();
        StartCoroutine(AutoConnect());
    }

    private void OnDestroy()
    {
        _client.Stop();   
    }             

    void Update()
    {
        _client.Update();
        Simulation.Update(Time.deltaTime * 1000);
    }            

    /// <summary>
    /// 连接服务器
    /// </summary>
    /// <returns></returns>
    public IEnumerator AutoConnect()
    {
        while (!Connected)
        {
            _client.Connect(ServerIp, ServerPort);
            yield return new WaitForSeconds(1);
        }

        yield return null;
    }
}     
